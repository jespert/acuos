#!/bin/bash

mkdir -p test.run
rm -rf test.run/*

for p in test.in/*.maude
do
	f=$(basename ${p})
	echo "Testing ${f}..."
	echo quit \
	| maude -no-wrap src/lgg.maude src/aux-test.maude ${p} \
	| grep -v Solution \
	> test.run/${f}.out \
    2> test.run/${f}.err
done
