fmod HIGHER-ORDER is
  sort HOperator .

  sort HTerm .
  subsort HOperator < HTerm .
  op _`[_`] : HOperator HTermList -> HTerm
              [prec 10] .
  op _`(_`) : HOperator HTermPair -> HTerm
              [prec 10] .

  sort HTermList .
  subsort HTerm < HTermList .
  op __ : HTermList HTermList -> HTermList
          [assoc prec 20] .

  sort HTermPair .
  op _;;_ : HTerm HTerm -> HTermPair
            [comm prec 20] .

  sort HConj .
  subsort HTerm < HConj .
  op _/\_ : HConj HConj -> HConj
            [assoc comm prec 30] .

  sort HRule .
  subsort HRule < HTerm .
  op _=>_ : HConj HTerm -> HRule
            [prec 40] .

  sort HModel .
  subsort HTerm < HModel .
  op none : -> HModel .
  op _;_ : HModel HModel -> HModel
           [assoc id: none prec 50] .

endfm

fmod SPECIFICATION is
  inc HIGHER-ORDER .

  *** G domain
  ops mass sun planet gravity : -> HOperator .

  *** E domain
  ops charge coulomb electron nucleus : -> HOperator .

  *** Shared by both
  ops attraction distant x y : -> HOperator .

endfm

parse distant ((#0:HOperator ;; #1:HOperator)) .

mod TEST-SPECIFICATION is
  inc ACU-LGG-RECURSIVE + SPECIFICATION .
endm


rew in TEST-SPECIFICATION :
    pretty-print-ts(
         upModule('SPECIFICATION, true),
         lggs(upModule('SPECIFICATION, true),
              upTerm(distant (sun ;; planet) ; mass [sun] ; mass [planet] ; mass [x] /\ mass [y] => gravity [x y] ; gravity [x y] => attraction [x y]),
              upTerm(distant (nucleus ;; electron) ; charge [electron] ; charge [nucleus] ; charge [x] /\ charge [y] => coulomb [x y] ; coulomb [x y] => attraction [x y]))) .
quit .