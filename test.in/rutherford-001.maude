fmod SPECIFICATION is
  sort HOperator .

  sort HTerm .
  subsort HOperator < HTerm .
  op _`[_`] : HOperator HTermList -> HTerm [prec 10].

  sort HTermList .
  subsort HTerm < HTermList .
  op __ : HTermList HTermList -> HTermList [assoc] .

  sort HConj .
  subsort HTerm < HConj .
  op _/\_ : HConj HConj -> HConj [assoc comm prec 40] .

  sort HRule .
  subsort HRule < HTerm .
  op _=>_ : HConj HTerm -> HRule [prec 30].

  sort HModel .
  subsort HTerm < HModel .
  op _;_ : HModel HModel -> HModel [assoc comm prec 50] .

  *** G domain
  ops mass sun planet gravity : -> HOperator .

  *** E domain
  ops charge coulomb electron nucleus : -> HOperator .

  *** Shared
  ops attraction distant x y : -> HOperator .

endfm

*** parse in SPECIFICATION : mass[sun] ; mass[planet] .
*** parse in SPECIFICATION : mass[sun] ; (mass[planet]) .
*** parse in SPECIFICATION : (mass[sun] ; mass)[planet] .
*** parse in SPECIFICATION : mass[sun] .
*** parse in SPECIFICATION : (mass[sun] ; mass) .

*** mass [sun] ; mass [planet] ; distant [sun planet] ; mass [x] /\ mass [y] => gravity [x y] ; gravity [x y] => attraction [x y]
*** charge [electron] ; charge [nucleus] ; distant [nucleus, electron] ; charge [x] /\ charge [y] => coulomb [x y] ; coulomb [x y] => attraction [x y]

mod TEST-SPECIFICATION is
  inc ACU-LGG-RECURSIVE + SPECIFICATION .
endm


rew in TEST-SPECIFICATION :
    pretty-print-ts(
         upModule('SPECIFICATION, true),
         lggs(upModule('SPECIFICATION, true),
              upTerm(mass [sun] ; mass [planet] ; distant [sun planet] ; mass [x] /\ mass [y] => gravity [x y] ; gravity [x y] => attraction [x y]),
              upTerm(charge [electron] ; charge [nucleus] ; distant [nucleus electron] ; charge [x] /\ charge [y] => coulomb [x y] ; coulomb [x y] => attraction [x y]))) .
quit .