mod PRETTY-PRINT is
	inc META-LEVEL-AUX .
***    inc MODULE-HANDLING .


	var tl tl0 tl1 tl2 tl3 tl4 tl5 tl6 TL : TermList .
	var v v1 v2 : Variable .
	var et pt t t1 t2 t3 t4 t5 t6 V T : Term .
	var cnt : Constant .
	var q q1 q2 q3 qidM F : Qid .
	var qs1 qs2 : QidSet .
	var gtl gtl1 : GTermList .
	var n n1 n2 n3 n4 n5 n6 n7 n8 n9 n10 n11 n12 nc nc1 nc2 nc3 : Nat .
	var tp : Type .
	var AtS AtS1 AtS2 : AttrSet .
  	var Cond Cond1 Cond2 B* Bp* Bs* : Condition .
	var C : Context .
	var typ typ1 typ2 typ3 : Type .
	var typl typl2 TPL : TypeList .
	var r r1 r2 : Rule .
	var m m1 m2 M : Module .
	var str1 str2 str3 str4 str5 str6 str7 str8 : String .
	vars b B : Bool .
        var MOD : Module .
        var TS : TermSet .

	op dt : TermList Module Bool -> String .
		eq dt(empty, m, b) = "" .
		ceq dt(v, m, b) = substr(string(v), 0, find(string(v), ":", 0)) if (substr(string(v), 0, 1) =/= "#") .
		eq dt(cnt, m, b) = substr(string(cnt), 0, find(string(cnt), ".", 0)) .
		ceq dt(t, m, b) = dtNat(t, b) if (substr(toString(t, b), 0, 4) == "'s_^") or (substr(toString(t, b), 0, 3) == "'s_") .
		eq dt(q[tl], m, b) = if assoc in axioms(m,q) then dt'(tl, q, m, b) else replace(cleanQid(string(q)), "_", tl, m, b) fi .
		eq dt(t, m, true) = if (substr(string(t), 0, 1) == "#") then "*" else string(t) fi .
		eq dt(t, m, false) = if (substr(string(t), 0, 1) == "#") then substr(string(t), 0, find(string(t), ":", 0)) else string(t) fi .

	op cleanQid : String -> String .
		eq cleanQid(str1) = replaceStr("`","",str1) .

	op replaceStr : String String String -> String .
		ceq replaceStr(str1, str3, str2) = replaceStr(str1, str3,substr(str2, 0, find(str2, str1, 0)) + str3 + substr(str2, find(str2, str1, 0) + length(str1), length(str2))) if (find(str2, str1, 0) =/= notFound) .
		eq replaceStr(str1, str3, str2) = str2 [owise] .

	op dt' : TermList Qid Module Bool -> String .
		eq dt'((t, empty), q, m, b) = dt(t, m, b) .
		eq dt'((t, tl), q, m, b) = if (find(string(q), "_", 0) =/= notFound) then
										dt(t, m, b) + " " + substr(string(q),find(string(q), "_", 0) + 1, sd(rfind(string(q), "_", length(string(q))),1)) + " " + dt'(tl, q, m, b)
									else
										dt(t, m, b) + " " + string(q) + " " + dt'(tl, q, m, b)
									fi .

	op dt'' : TermList Module Bool -> String .
		ceq dt''((t, empty), m, b) = dtNat(t, b) if (substr(toString(t, b), 0, 4) == "'s_^") or (substr(toString(t, b), 0, 3) == "'s_") .
		eq dt''((t, empty), m, b) = dt(t, m, b) .
		ceq dt''((t, tl), m, b) = dtNat(t, b) + "," + dt''(tl, m, b) if (substr(toString(t, b), 0, 4) == "'s_^") or (substr(toString(t, b), 0, 3) == "'s_") .
		eq dt''((t, tl), m, b) = dt(t, m, b) + "," + dt''(tl, m, b) .

	op dt''' : TermList Module Bool -> String .
		ceq dt'''(q[tl],m,b) = replace(string(q), "_", tl, m, b) if find(string(q),"_",find(string(q),"_",0) + 2) =/= notFound .
		eq dt'''(q[tl],m,b) = replaceStr("_", "(" + dt''(tl,m,false) + ")", string(q)) .

	op dtCond : Condition Module -> String .
		eq dtCond(nil, m) = "" .
		eq dtCond(q[tl] = 'true.Bool, m) = if (find(dt(q[tl],m,false), "#", 0) == notFound) then "true" else dt'''(q[tl],m,false) fi . --- si no hay # devolver true
		eq dtCond(Cond1 /\ Cond2, m) = dtCond(Cond1, m) + " and " + dtCond(Cond2, m) .
		eq dtCond(Cond1 /\ nil, m) = dtCond(Cond1, m) .

	op dtNat : Term Bool -> String .
		ceq dtNat(t, b) = substr(toString(t, b), find(toString(t, b), "^", 1) + 1, sd(find(toString(t, b), "^", 1) + 1,rfind(toString(t, b), "[", length(toString(t, b))))) if (substr(toString(t, b), 0, 4) == "'s_^") .
		ceq dtNat(t, b) = "1" if (substr(toString(t, b), 0, 3) == "'s_") .

	op replace : String String TermList Module Bool -> String .
		ceq replace(str1, str2, tl, m, b) = str1 + "(" + dt''(tl, m, b) + ")" if find(str1, str2,0) == notFound .
		eq replace(str1, str2, (t, empty), m, b) = if ((substr(toString(t, b), 0, 3) =/= "'s_") and (substr(toString(t, b), 0, 4) =/= "'s_^")) then
													substr(str1, 0, find(str1, str2,0)) + " " + dt(t, m, b) + " " + substr(str1, find(str1, str2,0) + 1, length(str1))
												else
													substr(str1, 0, find(str1, str2,0)) + " " + dtNat(t, b) + " " + substr(str1, find(str1, str2,0) + 1, length(str1))
												fi .
		eq replace(str1, str2, (t, tl), m, b) = if ((substr(toString(t, b), 0, 3) =/= "'s_") and (substr(toString(t, b), 0, 4) =/= "'s_^")) then
													replace(substr(str1, 0, find(str1, str2,0)) + " " + dt(t, m, b) + " " + substr(str1, find(str1, str2,0) + 1, length(str1)), str2, tl, m, b)
												else
													replace(substr(str1, 0, find(str1, str2,0)) + " " + dtNat(t, b) + " " + substr(str1, find(str1, str2,0) + 1, length(str1)), str2, tl, m, b)
												fi .

	op toString : TermList Bool -> String .
		eq toString(empty, b) = "" .
		eq toString(cnt, b) = "'" + string(cnt) .
		eq toString(q[tl], b) = "'" + string(q) + "[" + toString(tl, b) + "]" .
		eq toString(t, true) = if (vars(t) == t) then "*" else string(t) fi .
		eq toString(t, false) = "'" + string(t) .
		eq toString((t, tl), b) = toString(t, b) + "," + toString(tl, b) .


  **** vars returns the set of variables in a term
  op vars : Term -> QidSet .
  op vars : TermList -> QidSet .

  eq vars(V) = V .
  eq vars(C) = none .
  eq vars(F[TL]) = vars(TL) .
  eq vars(empty) = none .
  eq vars((T, TL)) = vars(T) ; vars(TL) .

  sort StringList .
  subsort String < StringList .
  op empty : -> StringList [ctor] .
  op _+++_ : StringList StringList -> StringList [assoc comm id: empty] .

  op down-term-set : Module TermSet Bool -> String .
  eq down-term-set(MOD, emptySet, b) = empty .
  eq down-term-set(MOD, (T ;; TS), b) = dt(T, MOD, b) +++ down-term-set(MOD, TS, b) .

endm