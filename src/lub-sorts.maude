fmod LUB-SORTS is
  pr META-LEVEL .

  var  MOD : Module .
  var  TYS : TypeSet .
  vars TY0 TY1 TY2 TY3 : Type .
  var  SORTS : TypeSet .

  *** Computes the least upper bound of two types
  ***
  *** For performance reasons, we restrict the search to types in the same kind,
  *** by using getKind and lesserSorts.
  op  lubSorts : Module Type Type -> TypeSet .
  eq  lubSorts(MOD, TY0, TY1) =
        if sameKind(MOD, TY0, TY1)
        then aux-lubSorts-filter-redundant(
               MOD,
               TY0,
               TY1,
               aux-lubSorts-filter-invalid(
                 MOD,
                 TY0,
                 TY1,
                 lesserSorts(MOD, getKind(MOD, TY0))
               )
             )
        else (none).EmptyTypeSet
        fi .
  
  *** Remove types which are not general enough to include both TY0 and TY1
  op  aux-lubSorts-filter-invalid : Module Type Type TypeSet -> TypeSet .
  ceq aux-lubSorts-filter-invalid(MOD, TY0, TY1, (TY2 ; SORTS)) =
      aux-lubSorts-filter-invalid(MOD, TY0, TY1, SORTS)
   if (not sortLeq(MOD, TY0, TY2)) or (not sortLeq(MOD, TY1, TY2)) .
  eq  aux-lubSorts-filter-invalid(MOD, TY0, TY1, SORTS) = SORTS [owise] .

  *** Remove types which are too general
  op  aux-lubSorts-filter-redundant : Module Type Type TypeSet -> TypeSet .
  ceq aux-lubSorts-filter-redundant(MOD, TY0, TY1, (TY2 ; TY3 ; SORTS)) =
        aux-lubSorts-filter-redundant(MOD, TY0, TY1, (TY2 ; SORTS))
   if sortLeq(MOD, TY2, TY3) .
  eq  aux-lubSorts-filter-redundant(MOD, TY0, TY1, SORTS) = SORTS [owise] .

endfm
